//
//  TableViewCell.swift
//  TraktTV
//
//  Created by GabrielMassana on 21/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

/// Protocol to handle common UITableViewCell func.
protocol TableViewCell {
    
    /**
     Help class function to return the cell reuseIdentifier.
     
     - Returns: a String object as a reuseIdentifier.
     */
    static func reuseIdentifier() -> String
    
    /// Help function to call auto-layout methods.
    func layoutByApplyingConstraints()
    
    /// The cell layer is rendered as a bitmap.
    func rasterizeLayer()
}

/// Default implementation for TableViewCell protocol
extension TableViewCell where Self: UITableViewCell {
    
    //MARK: - Identifier
    
    static func reuseIdentifier() -> String {
        
        return NSStringFromClass(self.self)
    }
    
    //MARK: - Layout
    
    func layoutByApplyingConstraints() {
        
        setNeedsUpdateConstraints()
        setNeedsLayout()
    }
    
    func rasterizeLayer() {
        
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}
