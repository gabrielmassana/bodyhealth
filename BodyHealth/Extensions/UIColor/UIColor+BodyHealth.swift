//
//  UIColor+BodyHealth.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//
//  Naming: http://chir.ag/projects/name-that-color/
//

import UIKit

import ColorWithHex

// Accessors for all the colors used.
extension UIColor {
    
    //MARK: - Gray
    
    @nonobjc static var scorpion: UIColor = {
        
        return UIColor.colorWithHex("606060")!
    }()
    
    @nonobjc static var dustyGray: UIColor = { // icons
        
        return UIColor.colorWithHex("9a9a9a")!
    }()
    
    @nonobjc static var alto: UIColor = {
        
        return UIColor.colorWithHex("DFDFDF")!
    }()
    
    @nonobjc static var concrete: UIColor = {
        
        return UIColor.colorWithHex("F2F2F2")!
    }()
    
    //MARK: - Orange
    
    @nonobjc static var buttercup: UIColor = {
        
        return UIColor.colorWithHex("F49F15")!
    }()
    
    @nonobjc static var meteor: UIColor = {
        
        return UIColor.colorWithHex("c68214")!
    }()
    
    //MARK: - Glue

    @nonobjc static var darkBlue: UIColor = {
        
        return UIColor.colorWithHex("0E04A9")!
    }()
    
    //MARK: - Red

    @nonobjc static var torchRed: UIColor = {
        
        return UIColor.colorWithHex("F5062D")!
    }()
    
    @nonobjc static var radicalRed: UIColor = {
        
        return UIColor.colorWithHex("FC3159")!
    }()
}
