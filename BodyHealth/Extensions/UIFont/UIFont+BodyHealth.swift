//
//  UIFont+BodyHealth.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

/**
 Accessors for all the fonts used.
 */
extension UIFont {
    
    class func sanFranciscoUITextBold(withSize size: CGFloat) -> UIFont {
        
        return UIFont(name: "SFUIText-Bold",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
    
    class func sanFranciscoUITextHeavy(withSize size: CGFloat) -> UIFont {
        
        return UIFont(name: "SFUIText-Heavy",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
    
    class func sanFranciscoUITextLight(withSize size: CGFloat) -> UIFont {
        
        return UIFont(name: "SFUIText-Light",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
    
    class func sanFranciscoUITextMedium(withSize size: CGFloat) -> UIFont {
        
        return UIFont(name: "SFUIText-Medium",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
    
    class func sanFranciscoUITextRegular(withSize size: CGFloat) -> UIFont {
        
        return UIFont(name: "SFUIText-Regular",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
    
    class func sanFranciscoUITextSemibold(withSize size: CGFloat) -> UIFont {
        
        return UIFont(name: "SFUIText-Semibold",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
    
    class func sanFranciscoUITextUltrathin(withSize size: CGFloat) -> UIFont {
        
        return UIFont(name: "SFUIText-Ultrathin",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
}
