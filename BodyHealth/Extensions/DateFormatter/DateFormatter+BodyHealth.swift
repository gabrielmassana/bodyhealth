//
//  DateFormatter+BodyHealth.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 10/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    @nonobjc static let datePickerFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "dMMMMHHmm",
                                                        options : 0,
                                                        locale: Locale.current)
        
        return formatter
    }()
    
    @nonobjc static let weekChartFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "d",
                                                        options : 0,
                                                        locale: Locale.current)
        
        return formatter
    }()
}
