//
//  Double+Rounded.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 11/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

extension Double {

    func roundTo(places: Int) -> Double {
        
        let divisor = pow(10.0, Double(places))
        
        return (self * divisor).rounded() / divisor
    }
}
