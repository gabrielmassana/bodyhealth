//
//  UIViewController+ChildViewController.swift
//  BodyHealth
//
//  Created by GabrielMassana on 31/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

/// Helps add and remove child view controllers.
extension UIViewController {
    
    /**
     Adds in self a view controller in a certain view frame.
     
     - Parameter viewController: The ViewController to be added as a child.
     - Parameter frame: Frame for the view controller view to be added as a child.
     - Parameter view: View where we are adding the child as subview.
     */
    func addChildViewController(_ viewController: UIViewController, frame: CGRect, view: UIView) {
        
        viewController.view.frame = frame
        view.addSubview(viewController.view)
        addChildViewController(viewController)
        viewController.didMove(toParentViewController: self)
    }
    
    /**
     Adds in self a view controller in a certain frame.
     
     - Parameter viewController: The ViewController to be added as a child.
     - Parameter frame: Frame for the view controller view to be added as a child.
     */
    func addChildViewController(_ viewController: UIViewController, frame: CGRect) {
        
        addChildViewController(viewController)
        viewController.view.frame = frame
        view.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
    }
    
    /**
     Remove self view controller as a child view controller from his actual parent.
     */
    func removeFromParentViewControllerAndSuperview() {
        
        willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
}
