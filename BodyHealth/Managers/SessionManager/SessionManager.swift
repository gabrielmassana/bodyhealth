//
//  SessionManager.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

enum SessionState {
    
    case unknown
    case closed
    case open
}

class SessionManager: NSObject {

    //MARK: - Singleton

    /**
     Singleton.
     
     - Returns: DeviceManager instance
     */
    static let sharedInstance = SessionManager()

    //MARK: - Accessors

    var state: SessionState = .unknown
    
    //MARK: - Init
    
    private override init() {
        
        super.init()
        
        loadSessionState()
    }
    
    //MARK: - SessionState
    
    func loadSessionState() {
        
        switch HealthKitManager.sharedInstance.authorizationStatus() {
            
        case .notDetermined,
             .sharingDenied,
             .dataUnavailable:
            
            state = .closed
            
        case .sharingAuthorized:
            
            state = .open
        }
    }

}
