//
//  UserDefaultsKeys.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 08/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

struct UserDefaultsKeys {
    
    static let weightUnitKey = "com.bodyhealth.WeightUnitKey.key"
    static let weightTimeKey = "com.bodyhealth.WeightTimeKey.key"
}
