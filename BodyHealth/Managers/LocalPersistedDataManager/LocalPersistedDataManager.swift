//
//  LocalPersistedDataManager.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 08/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

class LocalPersistedDataManager: NSObject {
    
    //MARK: - Accessors
    
    /// UserDefaults instance
    static var userDefaults: UserDefaults {
        
        let userDefaults = UserDefaults.standard
        
        return userDefaults
    }
    
    //MARK: - UserDefaults

    //MARK: WeightUnit

    static func saveWeightUnit(unit: String) {
        
        userDefaults.set(unit, forKey: UserDefaultsKeys.weightUnitKey)
        
        userDefaults.synchronize()
    }
    
    static func retrieveWeightUnit() -> String? {
    
        return userDefaults.object(forKey: UserDefaultsKeys.weightUnitKey) as? String
    }
    
    //MARK: WeightTimeKey
    
    static func saveWeightTime(time: ChartTimeUnit) {
        
        userDefaults.set(time.position(), forKey: UserDefaultsKeys.weightTimeKey)
        
        userDefaults.synchronize()
    }
    
    static func retrieveWeightTime() -> ChartTimeUnit? {
        
        let position = userDefaults.object(forKey: UserDefaultsKeys.weightTimeKey) as? Int
        
        return ChartTimeUnit.rawValue(value: position)
    }
}
