//
//  HealthKitManager.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import HealthKit

enum AuthorizationStatus {
    
    case dataUnavailable
    case notDetermined
    case sharingDenied
    case sharingAuthorized
}

enum TypeIdentifier {
    
    case bodyMass
    case height
    case bodyMassIndex
    
    func sampleType() -> HKQuantityType {
        
        switch self {
        case .bodyMass:

            return HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
            
        case .height:
            
            return HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!

        case .bodyMassIndex:

            return HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)!
        }
    }
}

class HealthKitManager: NSObject {

    //MARK: - Singleton
    
    /// Returns the global APIConfig instance.
    static let sharedInstance = HealthKitManager()
    
    //MARK: - Accessors

    lazy var healthKitStore: HKHealthStore = {
        
        var healthKitStore = HKHealthStore()
        
        return healthKitStore
    }()
    
    //MARK: - Init

    private override init() {
        
        super.init()
    }
    
    //MARK: - HealthKit
    
    func authorizeHealthKit(completion: ((_ success: Bool, _ error: NSError?) -> Void)!) {
        
        let typesToRead = Set<HKSampleType>(arrayLiteral: TypeIdentifier.bodyMass.sampleType(),
                                            TypeIdentifier.height.sampleType(),
                                            TypeIdentifier.bodyMassIndex.sampleType()
        )
        
        let typesToShare = Set<HKSampleType>(arrayLiteral: TypeIdentifier.bodyMass.sampleType(),
                                             TypeIdentifier.height.sampleType(),
                                             TypeIdentifier.bodyMassIndex.sampleType()
        )
        
        // 3. If the store is not available return an error and don't go on.
        if HKHealthStore.isHealthDataAvailable() {
            
            // 4.  Request HealthKit authorization
            healthKitStore.requestAuthorization(toShare: typesToShare, read: typesToRead) { (success, error) -> Void in
                                                    
                if let completion = completion {
                    
                    completion(success,
                               error as? NSError)
                }
            }
        }
        else {
            
            let error = NSError(domain: "com.healthkit.gabrielmassana",
                                code: 2,
                                userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in this Device"])
            
            if let completion = completion {
                
                completion(false,
                           error)
            }
        }
    }
    
    //MARK: - AuthorizationStatus

    func authorizationStatus() -> AuthorizationStatus {
        
        if HKHealthStore.isHealthDataAvailable() {
            
            if autorizarionStatusForType(.bodyMass) == .notDetermined ||
                autorizarionStatusForType(.height) == .notDetermined ||
                autorizarionStatusForType(.bodyMassIndex) == .notDetermined {
                
                return .notDetermined
            }
            else if autorizarionStatusForType(.bodyMass) == .sharingDenied ||
                autorizarionStatusForType(.height) == .sharingDenied ||
                autorizarionStatusForType(.bodyMassIndex) == .sharingDenied {
                
                return .sharingDenied
            }
            else {
                
                return .sharingAuthorized
            }
        }
        else {
            
            return .dataUnavailable
        }
    }
    
    func autorizarionStatusForType(_ typeIdentifier: TypeIdentifier) -> HKAuthorizationStatus {
        
        return HealthKitManager.sharedInstance.healthKitStore.authorizationStatus(for: typeIdentifier.sampleType())
    }
    
    //MARK: - Retrieve

    func retrieveMostRecentSample(sampleType: HKSampleType, completion: @escaping (HKSample?, Error?) -> Void) {
        
        let past = Date.distantPast
        let now   = Date()
        let predicate = HKQuery.predicateForSamples(withStart: past,
                                                    end: now,
                                                    options: [])
        
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate,
                                              ascending: false)
        
        let sampleQuery = HKSampleQuery(sampleType: sampleType,
                                        predicate: predicate,
                                        limit: 1,
                                        sortDescriptors: [sortDescriptor]) { query, results, error in
            
            
            
            // Get the first sample
            let mostRecentSample = results?.first as? HKQuantitySample
            
            // Execute the completion closure
                
            completion(mostRecentSample,
                       nil)
        }

        healthKitStore.execute(sampleQuery)
    }
    
    func retrieveSamples(sampleType: HKSampleType, period: ChartTimeUnit,  completion: @escaping (HKStatisticsCollection?) -> Void) {
        
        guard let quantityType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass) else {
            
            return
        }
        
        
        let calendar = Calendar.current
        var interval = DateComponents()
        interval.day = 1
        let calendarComponents = Set<Calendar.Component>([.day, .month, .year])
        var components = calendar.dateComponents(calendarComponents, from: Date())
        components.hour = 0
        
        guard  let anchorDate = calendar.date(from: components) else {

            return
        }
        
        // Define 1-day intervals starting from 0:00
        let stepsQuery = HKStatisticsCollectionQuery(quantityType: quantityType,
                                                     quantitySamplePredicate: nil,
                                                     options: .discreteAverage,
                                                     anchorDate: anchorDate,
                                                     intervalComponents: interval)
        
        // Set the results handler
        stepsQuery.initialResultsHandler = {
            query, results, error in
            
            completion(results)
        }
        
        healthKitStore.execute(stepsQuery)
    }
}
