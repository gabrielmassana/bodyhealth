//
//  WeightHealthKit.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 10/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

import HealthKit

class WeightHealthKit: NSObject {

    //MARK: - Upload
    
    class func uploadWeight(weight: Double, date: Date, weightUnit: WeightUnit, completion: @escaping (Bool) -> Void) {
        
        let type = TypeIdentifier.bodyMass.sampleType()
        
        let weightQuantity = weightUnit.unit(weight: weight)
        
        let weightSample = HKQuantitySample(type: type,
                                            quantity: weightQuantity,
                                            start: date,
                                            end: date)
        
        // Save in the store
        HealthKitManager.sharedInstance.healthKitStore.save(weightSample, withCompletion: { (success, error) -> Void in
            
            if( error != nil ) {
                
                print("Error saving sample: \(error?.localizedDescription)")
                completion(false)
                
            }
            else {
                
                print("Sample saved successfully!")
                completion(true)
            }
        })
    }
    
    //MARK: - Retrieve

    class func retrieveWeight(completion: @escaping (Double, String) -> Void) {
        
        guard let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass) else {
            
            completion(0.0,
                       NSLocalizedString("Unknown", comment: "unknown_title"))
            
            return
        }
        
        // Call to read the most recent weight
        HealthKitManager.sharedInstance.retrieveMostRecentSample(sampleType: sampleType, completion: { quantitySample, error -> Void in
            
            guard let weightQuantitySample = quantitySample as? HKQuantitySample,
                let persistedUnit = LocalPersistedDataManager.retrieveWeightUnit(),
                let weightUnit = WeightUnit(rawValue: persistedUnit) else {
                
                    completion(0.0,
                               NSLocalizedString("Unknown", comment: "unknown_title"))

                return
            }
            
            if let error = error {
                
                print("Error reading weight from HealthKit Store: \(error.localizedDescription)")
                
                completion(0.0,
                           NSLocalizedString("Unknown", comment: "unknown_title"))

                return
            }
            
            var weight = weightUnit.quantity(weightQuantity: weightQuantitySample.quantity)
            
            weight = weight.roundTo(places: 2)
            
            // Push to the main thread
            DispatchQueue.main.async(execute: { () -> Void in
                
                completion(weight,
                           "\(weight) \(persistedUnit)")

            })
        })
    }
    
    class func retrieveWeightSamples(period: ChartTimeUnit, completion: @escaping (([String]?, [Double]?) -> Void)) {
        
        guard let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass) else {
            
            completion(nil, nil)
            
            return
        }
        
        HealthKitManager.sharedInstance.retrieveSamples(sampleType: sampleType, period: period, completion: { (stadistics) -> Void in
            
            let endDate = Date()
            let calendar = Calendar.current

            let timeFrame = period.timeFrame()
            
            guard let startDate = calendar.date(byAdding: timeFrame.component,
                                                value: -timeFrame.value,
                                                to: endDate),
                let results = stadistics else {
                    
                    return
            }
            
            var timeValues = [String]()
            var weightValues = [Double]()
            
            results.enumerateStatistics(from: startDate, to: endDate) { statistic, stop in
                
                if let quantity = statistic.averageQuantity() {
                    
                    let weight = quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
                    let day = DateFormatter.weekChartFormatter.string(from: statistic.startDate)
    
                    timeValues.append(day)
                    weightValues.append(weight)
                }
                else {
                    
                    let day = DateFormatter.weekChartFormatter.string(from: statistic.startDate)

                    timeValues.append(day)
                    weightValues.append(Double.nan)
                }
            }
            
            // Push to the main thread
            DispatchQueue.main.async(execute: { () -> Void in
                
                completion(timeValues,
                           weightValues)
            })
        })
    }
}
