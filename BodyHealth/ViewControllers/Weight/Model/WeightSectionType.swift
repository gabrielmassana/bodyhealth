//
//  WeightSectionType.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 07/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

internal enum WeightSectionType {
    
    case main
    
    func sectionTitle() -> String {
        
        switch self {
            
        case .main:
            
            return NSLocalizedString("Weight", comment: "weight_title")
        }
    }
}
