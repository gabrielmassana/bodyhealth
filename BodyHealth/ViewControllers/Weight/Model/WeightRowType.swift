//
//  WeightRowType.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 07/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

internal enum WeightRowType {
    
    case weight
    case date
    case submitButton
    case chart
    
    func rowCell(delegate: WeightViewController, weightModel: WeightModel) -> UITableViewCell {
        
        switch self {
            
        case .weight:
            
            let cell = WeightCell(weightModel: weightModel)
            cell.layoutByApplyingConstraints()
            cell.delegate = delegate
            
            return cell
            
        case .date:
            
            let cell = DateCell(weightModel: weightModel)
            cell.layoutByApplyingConstraints()
            
            return cell
            
        case .submitButton:
            
            let cell = SubmitButtonCell()
            cell.layoutByApplyingConstraints()
            cell.delegate = delegate
            
            return cell
            
        case .chart:
            
            let cell = ChartCell()
            cell.layoutByApplyingConstraints()
            
            return cell
        }
    }
}
