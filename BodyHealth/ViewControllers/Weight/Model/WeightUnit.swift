//
//  WeightUnit.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 08/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

import HealthKit

enum WeightUnit: String {
    
    case kg = "kg"
    case lbs = "lbs"
    case st = "st"
        
    func quantity(weightQuantity: HKQuantity) -> Double {
        
        switch self {
            
        case .kg:
            
            return weightQuantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
            
        case .lbs:
            
            return weightQuantity.doubleValue(for: HKUnit.pound())
            
        case .st:
            
            return weightQuantity.doubleValue(for: HKUnit.stone())
        }
    }
    
    func unit(weight: Double) -> HKQuantity {
        
        switch self {
            
        case .kg:
            
            return HKQuantity(unit: HKUnit.gram(),
                              doubleValue: weight * 1000)
            
        case .lbs:
            
            return HKQuantity(unit: HKUnit.pound(),
                              doubleValue: weight)
            
        case .st:
            
            return HKQuantity(unit: HKUnit.stone(),
                              doubleValue: weight)
        }
    }
    
    func row() -> Int {
        
        switch self {
            
        case .kg:
            
            return 0
            
        case .lbs:
            
            return 1
            
        case .st:
            
            return 2
        }
    }
    
}
