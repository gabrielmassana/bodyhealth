//
//  WeightSection.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 07/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

internal struct WeightSection {
    
    var section: WeightSectionType
    var rows: [WeightRowType]
}
