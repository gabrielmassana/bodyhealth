//
//  ChartTimeUnit.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 20/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

enum ChartTimeUnit {
    
    case week
    case month
    case year
    
    func withStart(now: Date) -> Date {
        
        switch self {
        case .week:
            
            return Date(timeInterval: -60*60*24*7,
                        since: now)
            
        case .month:
            
            return Date(timeInterval: -60*60*24*30,
                        since: now)
            
        case .year:
            
            return Date(timeInterval: -60*60*24*365,
                        since: now)
        }
    }
    
    func position() -> Int {
    
        switch self {
        case .week:
            
            return 0
            
        case .month:
            
            return 1
            
        case .year:
            
            return 2
        }
    }
    
    static func rawValue(value: Int?) -> ChartTimeUnit? {
        
        if value == 0 {
            
            return .week
        }
        else if value == 1 {
            
            return .month
        }
        else if value == 2 {
            
            return .year
        }
        else {
            
            return nil
        }
    }
    
    func timeFrame() -> (component: Calendar.Component, value: Int) {
        
        switch self {
        case .week:
            
            return (.day, 7)
            
        case .month:
            
            return (.day, 30)
            
        case .year:
            
            return (.day, 365)
        }
    }
}
