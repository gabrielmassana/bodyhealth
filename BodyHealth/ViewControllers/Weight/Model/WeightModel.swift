//
//  WeightModel.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 10/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

class WeightModel {
    
    var date: Date?
    var weight: Double?
    
    func isWeightValid() -> Bool {
    
        return weight != nil
    }
}
