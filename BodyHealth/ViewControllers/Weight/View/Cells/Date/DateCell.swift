//
//  DateCell.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class DateCell: UITableViewCell, TableViewCell {

    //MARK: - Accessors
    
    var weightModel: WeightModel
    
    lazy var dateTextField : UITextField = {
        
        var dateTextField = UITextField.newAutoLayout()
        
        dateTextField.font = UIFont.sanFranciscoUITextRegular(withSize: 20.0)
        dateTextField.textAlignment = .right
        dateTextField.textColor = UIColor.scorpion
        dateTextField.tintColor = UIColor.clear
        dateTextField.adjustsFontSizeToFitWidth = true
        dateTextField.inputView = self.datePicker
        
        return dateTextField
    }()
    
    lazy var datePicker: UIDatePicker = {
        
        var datePicker = UIDatePicker()
        
        datePicker.addTarget(self,
                             action: #selector(datePickerDidChange(_:)),
                             for: .valueChanged)
        
        datePicker.maximumDate = Date()
        
        return datePicker
    }()
    
    lazy var separationLine: UIView = {
        
        var separationLine = UIView.newAutoLayout()
        
        separationLine.backgroundColor = UIColor.alto
        
        return separationLine
    }()
    
    lazy var iconImageView: UIImageView = {
        
        var iconImageView = UIImageView.newAutoLayout()
        
        iconImageView.image = UIImage(named: "icon-basic-calendar")
        
        return iconImageView
    }()
    
    //MARK: - Init
    
    init(weightModel: WeightModel) {
        
        self.weightModel = weightModel
        
        super.init(style: .default,
                   reuseIdentifier: nil)
        
        selectionStyle = .none
        
        contentView.addSubview(dateTextField)
        contentView.addSubview(separationLine)
        contentView.addSubview(iconImageView)
        
        handleDate(date: Date())
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        dateTextField.autoPinEdge(toSuperviewEdge: .left,
                                  withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        dateTextField.autoPinEdge(toSuperviewEdge: .right,
                                  withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        dateTextField.autoPinEdge(toSuperviewEdge: .top)
        
        dateTextField.autoSetDimension(.height,
                                         toSize: 70.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            dateTextField.autoPinEdge(toSuperviewEdge: .bottom)
        }
        /*-------------------*/
        
        iconImageView.autoSetDimensions(to: CGSize(width: 24.0 * DeviceManager.sharedInstance.resizeFactor,
                                                   height: 24.0 * DeviceManager.sharedInstance.resizeFactor))
        
        iconImageView.autoAlignAxis(.horizontal,
                                    toSameAxisOf: dateTextField)
        
        iconImageView.autoPinEdge(toSuperviewEdge: .left,
                                  withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        separationLine.autoPinEdge(toSuperviewEdge: .top)
        
        separationLine.autoSetDimension(.height,
                                        toSize: 1 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdge(toSuperviewEdge: .right,
                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdge(toSuperviewEdge: .left,
                                   withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        super.updateConstraints()
    }
    
    //MARK: - DatePicker

    func datePickerDidChange(_ sender: UIDatePicker) {
        
        handleDate(date: sender.date)
    }
    
    //MARK: - Date
    
    func handleDate(date: Date) {
        
        self.update(withTextField: dateTextField, date: date)
        self.updateModel(withDate: date)
    }
    
    func update(withTextField textField: UITextField, date: Date) {
        
        textField.text = DateFormatter.datePickerFormatter.string(from: date)
    }
    
    func updateModel(withDate date: Date) {
        
        // Update Model
        weightModel.date = date
    }
}
