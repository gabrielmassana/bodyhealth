//
//  WeightCell+UITextFieldDelegate.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 09/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

extension WeightCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let text = textField.text {
            
            // Update Model
            updateModel(withWeight: text)
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            
            let textFieldText = text as NSString
            let text = textFieldText.replacingCharacters(in: range,
                                                         with: string)
            
            // Update Model
            updateModel(withWeight: text)
        }
        
        return true
    }
}
