//
//  WeightCell+UIPickerViewDataSource.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 08/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

extension WeightCell: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        return units.count
    }
}

