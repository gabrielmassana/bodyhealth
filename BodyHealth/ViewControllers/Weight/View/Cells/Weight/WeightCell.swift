//
//  WeightCell.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import PureLayout

protocol WeightCellDelegate: class {
    
    func weight(isValid valid: Bool)
}

class WeightCell: UITableViewCell, TableViewCell {

    //MARK: - Accessors
    
    weak var delegate: WeightCellDelegate?

    var weightModel: WeightModel

    var units: [WeightUnit] = {
        
        var units = [WeightUnit.kg, WeightUnit.lbs, WeightUnit.st]
        
        return units
    }()
    
    lazy var pickerView: UIPickerView = {
        
        var pickerView = UIPickerView()
        
        pickerView.showsSelectionIndicator = true
        pickerView.dataSource = self
        pickerView.delegate = self
        
        return pickerView
    }()
    
    lazy var weightTextField: UITextField = {
       
        var weightTextField = UITextField.newAutoLayout()
        
        weightTextField.font = UIFont.sanFranciscoUITextBold(withSize: 60.0)
        weightTextField.textAlignment = .center
        weightTextField.textColor = UIColor.buttercup
        weightTextField.tintColor = UIColor.buttercup
        weightTextField.keyboardType = .numbersAndPunctuation
        weightTextField.adjustsFontSizeToFitWidth = true
        weightTextField.minimumFontSize = 10.0
        weightTextField.returnKeyType = .send
        weightTextField.delegate = self
        weightTextField.attributedPlaceholder = NSAttributedString(string: "0.0",
                                                                   attributes: [NSForegroundColorAttributeName : UIColor.concrete])
        
        return weightTextField
    }()
    
    lazy var unitLabel: UILabel = {
        
        var unitLabel = UILabel.newAutoLayout()
        
        unitLabel.font = UIFont.sanFranciscoUITextSemibold(withSize: 30.0)
        unitLabel.textAlignment = .right
        unitLabel.textColor = UIColor.buttercup
        unitLabel.text = self.retrieveUnit()
        unitLabel.backgroundColor = UIColor.clear
        
        return unitLabel
    }()
    
    lazy var iconImageView: UIImageView = {
    
        var iconImageView = UIImageView.newAutoLayout()
        
        iconImageView.image = UIImage(named: "icon-body-weight")
        
        return iconImageView
    }()
    
    lazy var lastWeightLabel: UILabel = {
        
        var lastWeightLabel = UILabel.newAutoLayout()
        
        lastWeightLabel.font = UIFont.sanFranciscoUITextLight(withSize: 14.0)
        lastWeightLabel.textAlignment = .left
        lastWeightLabel.textColor = UIColor.scorpion
        lastWeightLabel.backgroundColor = UIColor.clear
        
        return lastWeightLabel
    }()
    
    lazy var unitTextField: UITextField = {
        
        var unitTextField = UITextField.newAutoLayout()

        unitTextField.inputView = self.pickerView
        unitTextField.tintColor = UIColor.clear
        
        return unitTextField
    }()
    
    lazy var separationLine: UIView = {
        
        var separationLine = UIView.newAutoLayout()
        
        separationLine.backgroundColor = UIColor.alto
        
        return separationLine
    }()
    
    //MARK: - Init
    
    init(weightModel: WeightModel) {
        
        self.weightModel = weightModel
        
        super.init(style: .default,
                   reuseIdentifier: nil)
        
        selectionStyle = .none
        
        contentView.addSubview(weightTextField)
        contentView.addSubview(separationLine)
        contentView.addSubview(unitLabel)
        contentView.addSubview(unitTextField)
        contentView.addSubview(lastWeightLabel)
        contentView.addSubview(iconImageView)

        /*-------------------*/
        
        showLastWeight()
        updatePickerRow()
        
        /*-------------------*/

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: NSNotification.Name.UIKeyboardWillShow ,
                                               object: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {

        weightTextField.autoPinEdge(.left,
                                    to: .right,
                                    of: iconImageView,
                                    withOffset: 5.0 * DeviceManager.sharedInstance.resizeFactor)
        
        weightTextField.autoPinEdge(.right,
                                    to: .left,
                                    of: unitLabel,
                                    withOffset: 0.0)
        
        weightTextField.autoPinEdge(toSuperviewEdge: .top,
                                    withInset: 20.0 * DeviceManager.sharedInstance.resizeFactor)
        
        weightTextField.autoSetDimension(.height,
                                         toSize: 70.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            weightTextField.autoPinEdge(toSuperviewEdge: .bottom,
                                        withInset: 5.0 * DeviceManager.sharedInstance.resizeFactor)
        }
        
        /*-------------------*/

        iconImageView.autoSetDimensions(to: CGSize(width: 24.0 * DeviceManager.sharedInstance.resizeFactor,
                                                   height: 24.0 * DeviceManager.sharedInstance.resizeFactor))
        
        iconImageView.autoAlignAxis(.horizontal,
                                    toSameAxisOf: weightTextField)
        
        iconImageView.autoPinEdge(toSuperviewEdge: .left,
                                  withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        separationLine.autoPinEdge(toSuperviewEdge: .top)
        
        separationLine.autoSetDimension(.height,
                                        toSize: 1 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdge(toSuperviewEdge: .right,
                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdge(toSuperviewEdge: .left,
                                   withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/

        lastWeightLabel.autoPinEdge(.top,
                                    to: .bottom,
                                    of: separationLine,
                                    withOffset: 5.0 * DeviceManager.sharedInstance.resizeFactor)
        
        lastWeightLabel.autoPinEdge(toSuperviewEdge: .right,
                                    withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/

        unitLabel.autoAlignAxis(.horizontal,
                                toSameAxisOf: weightTextField)

        unitLabel.autoPinEdge(toSuperviewEdge: .right,
                              withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)

        unitLabel.autoPinEdge(.left,
                              to: .left,
                              of: unitTextField)
        
        /*-------------------*/
        
        unitTextField.autoSetDimension(.width,
                                       toSize: 60.0 * DeviceManager.sharedInstance.resizeFactor)
        
        unitTextField.autoPinEdge(toSuperviewEdge: .bottom)
        
        unitTextField.autoPinEdge(toSuperviewEdge: .top)
        
        unitTextField.autoPinEdge(toSuperviewEdge: .right)

        /*-------------------*/

        super.updateConstraints()
    }
    
    //MARK: - Unit

    func retrieveUnit() -> String {
        
        var unit = WeightUnit.kg.rawValue
        
        if let storedUnit = LocalPersistedDataManager.retrieveWeightUnit() {
            
            unit = storedUnit
        }
        else {
            
            LocalPersistedDataManager.saveWeightUnit(unit: unit)
        }
        
        return unit
    }
    
    //MARK: - UpdateUnit

    func unitDidChange(unit: WeightUnit) {
        
        guard let persistedUnit = LocalPersistedDataManager.retrieveWeightUnit(),
            let oldUnit = WeightUnit(rawValue: persistedUnit) else {
        
                return
        }
        
        // Update WeightCell
        updateWith(newUnit: unit,
                   from: oldUnit)
        
        // Save value on NSUSerDefaults
        LocalPersistedDataManager.saveWeightUnit(unit: unit.rawValue)
        
        // Update Label
        unitLabel.text = unit.rawValue
    }

    //MARK: - UpdateCell

    func updateWith(newUnit unit: WeightUnit, from oldUnit: WeightUnit) {
        
        showLastWeight()
        
        updateWeightTextFieldWithNewUnit(newUnit: unit,
                                         from: oldUnit)
    }
    
    func showLastWeight() {
        
        lastWeight(completion: { (weight, lastWeightText) in
            
            self.lastWeightLabel.text = lastWeightText
        })
    }
    
    func updateWeightTextFieldWithNewUnit(newUnit unit: WeightUnit, from oldUnit: WeightUnit) {
        
        if weightModel.isWeightValid() == true,
            let weight = weightModel.weight {
                
            let weightQuantity = oldUnit.unit(weight: weight)
            let newWeightWithNewUnit = String(unit.quantity(weightQuantity: weightQuantity).roundTo(places: 2))
            
            weightTextField.text = newWeightWithNewUnit
            updateModel(withWeight: newWeightWithNewUnit)
        }
    }
    
    //MARK: - Data
    
    func lastWeight(completion: @escaping (String, String) -> Void) {
        
        let last = NSLocalizedString("Last: ", comment: "last_title")
        
        WeightHealthKit.retrieveWeight { (value, weight) in
            
            completion("\(value)",
                "\(last)\(weight)")
        }
    }
    
    func updateModel(withWeight weight: String) {
        
        // Update Model
        weightModel.weight = Double(weight)
        
        updateTextFieldValidState(isValid: weightModel.isWeightValid(),
                                  weight: weight)
    }
    
    //MARK: - ValidWeight

    func updateTextFieldValidState(isValid: Bool, weight: String) {

        delegate?.weight(isValid: isValid)
        
        if isValid == true ||
            weight == "" {
            
            weightTextField.textColor = UIColor.buttercup
            weightTextField.tintColor = UIColor.buttercup
            unitLabel.textColor = UIColor.buttercup
        }
        else {
            
            weightTextField.textColor = UIColor.torchRed
            weightTextField.tintColor = UIColor.torchRed
            unitLabel.textColor = UIColor.torchRed
        }
    }
    
    //MARK: - UpdatePickerRow
    
    func updatePickerRow() {
     
        let unit = retrieveUnit()
        
        if let weightUnit = WeightUnit(rawValue: unit) {
            
            let row = weightUnit.row()
            
            pickerView.selectRow(row,
                                 inComponent: 0,
                                 animated: false)
        }
    }
    
    //MARK: - Notification

    func keyboardWillShow(_ sender: Notification) {
        
        updatePickerRow()
    }
    
    //MARK: - Deinit

    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }
}
