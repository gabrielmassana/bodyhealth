//
//  SubmitButtonCell.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import ButtonBackgroundColor

protocol SubmitButtonCellDelegate: class {
    
    func didPressAddbutton()
}

class SubmitButtonCell: UITableViewCell, TableViewCell {
    
    //MARK: - Accessors
    
    weak var delegate: SubmitButtonCellDelegate?
    
    lazy var submitButton: UIButton = {
        
        var submitButton = UIButton.newAutoLayout()
        
        submitButton.backgroundColorForStates(normal: UIColor.buttercup,
                                           highlighted: UIColor.meteor)

        submitButton.setTitle(NSLocalizedString("Add", comment: "add_title"),
                           for: .normal)
        
        submitButton.setTitleColor(UIColor.white,
                                for: .normal)
        
        submitButton.layer.cornerRadius = 5.0 * DeviceManager.sharedInstance.resizeFactor
        
        submitButton.addTarget(self,
                            action: #selector(submitButtonPressed(_:)),
                            for: .touchUpInside)
        
        submitButton.titleLabel?.font = UIFont.sanFranciscoUITextRegular(withSize: 24.0)
        submitButton.isUserInteractionEnabled = false
        submitButton.isEnabled = false
        submitButton.alpha = 0.5
        
        submitButton.setBackgroundImage(nil, for: .selected)
        
        return submitButton
    }()
    
    lazy var successLabel: PaddingLabel = {
       
        var successLabel = PaddingLabel.newAutoLayout()
        
        successLabel.font = UIFont.sanFranciscoUITextRegular(withSize: 20.0)
        successLabel.text = NSLocalizedString("Weight saved successfully", comment: "success_uploading_weight")
        successLabel.textColor = UIColor.buttercup
        successLabel.numberOfLines = 0
        successLabel.minimumScaleFactor = 0.3
        successLabel.adjustsFontSizeToFitWidth = true
        successLabel.textAlignment = .center
        successLabel.isHidden = true
        
        successLabel.layer.cornerRadius = 5.0 * DeviceManager.sharedInstance.resizeFactor
        successLabel.layer.borderColor = UIColor.buttercup.cgColor
        successLabel.layer.borderWidth = 1.0 * DeviceManager.sharedInstance.resizeFactor
        
        return successLabel
    }()
    
    lazy var separationLine: UIView = {
        
        var separationLine = UIView.newAutoLayout()
        
        separationLine.backgroundColor = UIColor.alto
        
        return separationLine
    }()
    
    //MARK: - Init
    
    init() {
        
        super.init(style: .default,
                   reuseIdentifier: nil)
        
        selectionStyle = .none
        
        contentView.addSubview(submitButton)
        contentView.addSubview(separationLine)
        contentView.addSubview(successLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        submitButton.autoPinEdge(toSuperviewEdge: .left,
                              withInset: 165.0 * DeviceManager.sharedInstance.resizeFactor)
        
        submitButton.autoPinEdge(toSuperviewEdge: .right,
                              withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        submitButton.autoPinEdge(toSuperviewEdge: .top,
                              withInset: 20.0 * DeviceManager.sharedInstance.resizeFactor)
        
        submitButton.autoSetDimension(.height,
                                   toSize: 50.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            submitButton.autoPinEdge(toSuperviewEdge: .bottom,
                                  withInset: 20.0 * DeviceManager.sharedInstance.resizeFactor)

        }
        
        /*-------------------*/
        
        successLabel.autoPinEdge(toSuperviewEdge: .right,
                                 withInset: 165.0 * DeviceManager.sharedInstance.resizeFactor)
        
        successLabel.autoPinEdge(toSuperviewEdge: .left,
                                 withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        successLabel.autoPinEdge(toSuperviewEdge: .top,
                                 withInset: 20.0 * DeviceManager.sharedInstance.resizeFactor)
        
        successLabel.autoSetDimension(.height,
                                      toSize: 50.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        separationLine.autoPinEdge(toSuperviewEdge: .top)
        
        separationLine.autoSetDimension(.height,
                                        toSize: 1 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdge(toSuperviewEdge: .right,
                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdge(toSuperviewEdge: .left,
                                   withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        super.updateConstraints()
    }
    
    //MARK: - ButtonActions
    
    func submitButtonPressed(_ sender: UIButton) {
        
        self.delegate?.didPressAddbutton()
    }
    
    //MARK: - Message

    func showDidUploadWeightMessage(withSuccess success: Bool) {
    
        self.successLabel.alpha = 0.0
        self.successLabel.isHidden = false

        if success == true {
            
            UIView.animate(withDuration: 0.5, animations: {

                self.successLabel.alpha = 1.0

            }, completion: { (complete) in

                UIView.animate(withDuration: 0.25,
                               delay: 2.0,
                               options: UIViewAnimationOptions.curveLinear,
                               animations: {
                                
                                self.successLabel.alpha = 0.0

                }, completion: { (complete) in
                
                    self.successLabel.alpha = 1.0
                    self.successLabel.isHidden = true
                })
            })
        }
    }
}
