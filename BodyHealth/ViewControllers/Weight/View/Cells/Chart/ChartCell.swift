//
//  ChartCell.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 16/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import Charts

public class ChartFormatter: NSObject, IAxisValueFormatter {
    
    var values = [String]()
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        if values.indices.contains(Int(value)) == true {
        
            return values[Int(value)]
        }
        else {
            
            return ""
        }
    }
    
    public func setValues(values: [String]) {
        
        self.values = values
    }
}

class ChartCell: UITableViewCell, TableViewCell {

    //MARK: - Accessors

    lazy var lineChartView: LineChartView = {
        
        var lineChartView = LineChartView.newAutoLayout()
        
        lineChartView.noDataText = "No Data"
        lineChartView.backgroundColor = UIColor.concrete
        lineChartView.borderLineWidth = 1.0 * DeviceManager.sharedInstance.resizeFactor
        lineChartView.doubleTapToZoomEnabled = false
        lineChartView.drawBordersEnabled = true
        lineChartView.borderColor = UIColor.buttercup
        lineChartView.chartDescription?.text = ""
        lineChartView.drawGridBackgroundEnabled = true
        lineChartView.gridBackgroundColor = UIColor.white
        lineChartView.highlightPerDragEnabled = false
        
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.granularityEnabled = true
        lineChartView.xAxis.granularity = 1.0
        lineChartView.xAxis.decimals = 0
        lineChartView.xAxis.drawAxisLineEnabled = false
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.leftAxis.axisLineColor = UIColor.clear
        lineChartView.rightAxis.axisLineColor = UIColor.clear
        
        return lineChartView
    }()
    
    lazy var segmentedControl: UISegmentedControl = {
        
        var segmentedControl = UISegmentedControl.newAutoLayout()
        
        segmentedControl.insertSegment(withTitle: "Week",
                                       at: 0,
                                       animated: false)
        
        segmentedControl.insertSegment(withTitle: "Month",
                                       at: 1,
                                       animated: false)
        
        segmentedControl.insertSegment(withTitle: "Year",
                                       at: 2,
                                       animated: false)
        
        segmentedControl.addTarget(self,
                                   action: #selector(segmentedControlChanged(_:)),
                                   for: UIControlEvents.valueChanged)
        
        segmentedControl.selectedSegmentIndex = self.retrieveUnitSelectedSegment()

        segmentedControl.tintColor = UIColor.buttercup
        
        return segmentedControl
    }()

    lazy var separationLine: UIView = {
        
        var separationLine = UIView.newAutoLayout()
        
        separationLine.backgroundColor = UIColor.alto
        
        return separationLine
    }()
    
    //MARK: - Init
    
    init() {
        
        super.init(style: .default,
                   reuseIdentifier: nil)
        
        selectionStyle = .none
        
        contentView.addSubview(lineChartView)
        contentView.addSubview(segmentedControl)
        contentView.addSubview(separationLine)
        
        contentView.backgroundColor = UIColor.concrete
        
        loadChart(period: retrieveChartTimeUnit())
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        let dataEntries = dataEntriesWith(dataPoints: dataPoints,
                                          values: values)
        
        if dataEntries.count > 0 {
            
            let lineChartDataSet = lineChartDataSetWith(values: dataEntries,
                                                        label: "Weight")
            
            let lineChartData = LineChartData(dataSets: [lineChartDataSet])
            lineChartView.data = lineChartData
            
            let xaxis = XAxis()
            let formatter = ChartFormatter()
            
            xaxis.valueFormatter = formatter
            formatter.setValues(values: dataPoints)
            
            lineChartView.xAxis.valueFormatter = xaxis.valueFormatter
            lineChartView.xAxis.axisMaximum = Double(retrieveChartTimeUnit().timeFrame().value)
        }
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        segmentedControl.autoPinEdge(toSuperviewEdge: .left,
                                     withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        segmentedControl.autoPinEdge(toSuperviewEdge: .right,
                                     withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        segmentedControl.autoPinEdge(toSuperviewEdge: .top,
                                     withInset: 5.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/

        lineChartView.autoPinEdge(toSuperviewEdge: .left)
        
        lineChartView.autoPinEdge(toSuperviewEdge: .right)
        
        lineChartView.autoPinEdge(.top,
                                  to: .bottom,
                                  of: segmentedControl,
                                  withOffset: 0.0)
        
        lineChartView.autoSetDimension(.height,
                                        toSize: 220.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            lineChartView.autoPinEdge(toSuperviewEdge: .bottom)
            
        }

        /*-------------------*/
        
        separationLine.autoPinEdge(toSuperviewEdge: .top)
        
        separationLine.autoSetDimension(.height,
                                        toSize: 1 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdge(toSuperviewEdge: .right,
                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdge(toSuperviewEdge: .left,
                                   withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        super.updateConstraints()
    }
    
    //MARK: - SegmentedControlActions
    
    func segmentedControlChanged(_ sender: UISegmentedControl) {
        
        if let time = ChartTimeUnit.rawValue(value: sender.selectedSegmentIndex) {
            
            LocalPersistedDataManager.saveWeightTime(time: time)
            
            loadChart(period: time)
        }
    }
    
    //MARK: - LoadChart

    func loadChart(period: ChartTimeUnit) {
        
        WeightHealthKit.retrieveWeightSamples(period: retrieveChartTimeUnit(), completion: { times, weights -> Void in
            
            if let times = times,
                let weights = weights {
                
                self.setChart(dataPoints: times,
                              values: weights)
            }
        })
    }
    
    //MARK: - ChartData

    func dataEntriesWith(dataPoints: [String], values: [Double]) -> [ChartDataEntry] {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            
            if values[i].isNaN == false {
                
                let dataEntry = ChartDataEntry(x: Double(i),
                                               y: values[i])
                
                dataEntries.append(dataEntry)
            }
        }
        
        return dataEntries
    }
    
    func lineChartDataSetWith(values: [ChartDataEntry]?, label: String?) -> LineChartDataSet {
        
        let lineChartDataSet = LineChartDataSet(values: values,
                                                label: label)
        
        lineChartDataSet.fillColor = UIColor.buttercup
        lineChartDataSet.circleColors = [UIColor.buttercup]
        lineChartDataSet.circleRadius = 4.0 * DeviceManager.sharedInstance.resizeFactor
        lineChartDataSet.colors = [UIColor.buttercup]
        lineChartDataSet.valueTextColor = UIColor.scorpion
        
        return lineChartDataSet
    }
    
    //MARK: - Time
    
    func retrieveUnitSelectedSegment() -> Int {
        
        return retrieveChartTimeUnit().position()
    }
    
    func retrieveChartTimeUnit() -> ChartTimeUnit {
        
        var time = ChartTimeUnit.week
        
        if let storedUnit = LocalPersistedDataManager.retrieveWeightTime() {
            
            time = storedUnit
        }
        else {
            
            LocalPersistedDataManager.saveWeightTime(time: time)
        }
        
        return time
    }
}
