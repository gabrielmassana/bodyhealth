//
//  WeightViewController+Logic.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 13/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

extension WeightViewController {
    
    //MARK: - SubmitButtonIsEnabled
    
    func submitButtonCanUploadWeight(canUpload: Bool) {
        
        if let cell = tableView.cellForRow(at: IndexPath(row: indexForRowType(type: .submitButton),
                                                         section: indexForSectionType(type: .main))) as? SubmitButtonCell {
            
            cell.submitButton.isUserInteractionEnabled = canUpload
            cell.submitButton.isEnabled = canUpload
            
            if canUpload == true {
                
                cell.submitButton.alpha = 1.0
            }
            else {
                
                cell.submitButton.alpha = 0.5
            }
        }
    }
    
    //MARK: - UploadWeightSuccess
    
    func didUploadWeight(success: Bool) {
        
        if let cell = tableView.cellForRow(at: IndexPath(row: indexForRowType(type: .submitButton),
                                                         section: indexForSectionType(type: .main))) as? SubmitButtonCell {
            
            cell.showDidUploadWeightMessage(withSuccess: success)
        }
    }
    
    func cleanWeightValue() {
        
        if let cell = tableView.cellForRow(at: IndexPath(row: indexForRowType(type: .weight),
                                                         section: indexForSectionType(type: .main))) as? WeightCell {
            
            cell.weightTextField.text = ""
        }
        
        submitButtonCanUploadWeight(canUpload: false)
    }
    
    func updateLastWeight() {
        
        if let cell = tableView.cellForRow(at: IndexPath(row: indexForRowType(type: .weight),
                                                         section: indexForSectionType(type: .main))) as? WeightCell {
            
            cell.showLastWeight()
        }
    }
    
    func reloadChart() {
        
        if let cell = tableView.cellForRow(at: IndexPath(row: indexForRowType(type: .chart),
                                                         section: indexForSectionType(type: .main))) as? ChartCell {
            
            cell.loadChart(period: cell.retrieveChartTimeUnit())
        }
    }
    
    func dismissKeyboard() {
        
        if let parentViewController = parent as? BodyHealthScrollViewController {
            
            parentViewController.containerScrollView.endEditing(true)
        }
    }
    
    //MARK: - CellForType
    
    func indexForSectionType(type: WeightSectionType) -> Int {
        
        let sectionMain = sections.enumerated().filter({ $0.element.section == .main })

        guard let section = sectionMain.first else {
            
            return 0
        }
        
        return section.offset
    }
    
    func indexForRowType(type: WeightRowType) -> Int {
        
        let sectionsFiltered = sections.filter({ $0.section == .main })
        
        guard let sectionWeight = sectionsFiltered.first,
            let row = sectionWeight.rows.index(of: type) else {
                
                return 0
        }
        
        return row
    }
}
