//
//  WeightViewController+WeightCellDelegate.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 11/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

extension WeightViewController: WeightCellDelegate {
    
    func weight(isValid valid: Bool) {
        
        submitButtonCanUploadWeight(canUpload: valid)
    }
}
