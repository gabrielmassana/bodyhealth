//
//  WeightViewController+UITableViewDataSource.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 07/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

extension WeightViewController: UITableViewDataSource {
    
    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sections[section].rows.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rowType = sections[indexPath.section].rows[indexPath.row]
        
        return rowType.rowCell(delegate: self,
                               weightModel: weightModel)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let sectionType = sections[section].section
        
        return sectionType.sectionTitle()
    }
}
