//
//  WeightViewController+SubmitButtonCellDelegate.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 10/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

import HealthKit

extension WeightViewController: SubmitButtonCellDelegate {
    
    func didPressAddbutton() {
        
        guard let date = weightModel.date,
            let weight = weightModel.weight,
            let persistedUnit = LocalPersistedDataManager.retrieveWeightUnit(),
            let weightUnit = WeightUnit(rawValue: persistedUnit) else {
            
                return
        }
        
        WeightHealthKit.uploadWeight(weight: weight, date: date, weightUnit: weightUnit) { (success) in
                   
            DispatchQueue.main.async {
                
                self.didUploadWeight(success: success)
                
                if success == true {
                    
                    self.cleanWeightValue()
                    self.updateLastWeight()
                    self.reloadChart()
                    self.dismissKeyboard()
                }
            }
        }
    }
}
