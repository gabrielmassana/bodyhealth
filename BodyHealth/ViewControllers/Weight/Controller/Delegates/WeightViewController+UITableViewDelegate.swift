//
//  WeightViewController+UITableViewDelegate.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 07/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

extension WeightViewController: UITableViewDelegate {
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let height = heightAtIndexPath[indexPath]
        
        if let height = height {
            
            return CGFloat(height)
        }
        else {
            
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height: NSNumber = NSNumber(value: Float(cell.frame.height))
        heightAtIndexPath[indexPath] = height
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height: NSNumber = NSNumber(value: Float(cell.frame.height))
        heightAtIndexPath[indexPath] = height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 35.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0.01
    }
}
