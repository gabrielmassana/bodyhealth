//
//  WeightViewController.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class WeightViewController: UIViewController {

    //MARK: - ModelAccessors

    var heightAtIndexPath = [IndexPath : NSNumber]()
    
    var sections: [WeightSection] = {
        
        var sections = [
            WeightSection(section: .main, rows: [.weight, .date, .submitButton, .chart]),
            ]
        
        return sections
    }()
    
    //MARK: - Accessors

    var weightModel: WeightModel = {
        
        var weightModel = WeightModel()
        
        return weightModel
    }()
    
    /// Table view to display the data.
    lazy var tableView: UITableView = {
        
        let tableView: UITableView = UITableView(frame: CGRect.zero,
                                                 style: .grouped)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.onDrag
        
        return tableView
    }()
    
    //MARK: - Init
    
    init() {
        
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - ViewLifeCycle

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        view.addSubview(tableView)
        
        /*-------------------*/
        
        updateViewConstraints()
        
        /*-------------------*/

        view.backgroundColor = UIColor.white
    }
    
    //MARK: - Constraints
    
    override func updateViewConstraints() {
        
        super.updateViewConstraints()
        
        /*-------------------*/
        
        tableView.autoPinEdge(toSuperviewEdge: .left)
        
        tableView.autoSetDimension(.width,
                                   toSize: 320.0 * DeviceManager.sharedInstance.resizeFactor)
        
        tableView.autoPinEdge(toSuperviewEdge: .bottom)
        
        tableView.autoPinEdge(toSuperviewEdge: .top,
                              withInset: 20.0 * DeviceManager.sharedInstance.resizeFactor)
    }
}
