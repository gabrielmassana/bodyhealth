//
//  BodyMassIndexViewController.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class BodyMassIndexViewController: UIViewController {

    //MARK: - ViewLifeCycle

    override func viewDidLoad() {
        
        super.viewDidLoad()

        view.backgroundColor = UIColor.lightGray
    }
}
