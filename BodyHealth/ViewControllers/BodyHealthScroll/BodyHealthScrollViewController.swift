//
//  BodyHealthScrollViewController.swift
//  BodyHealth
//
//  Created by GabrielMassana on 25/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import PureLayout

enum HealthViewControllers: Int {
    
    case weight = 0
    case bmi = 1
    case height = 2
    
    func healthViewController() -> UIViewController {
        
        switch self {
        case .weight:
            
            return WeightViewController()
            
        case .bmi:
            
            return BodyMassIndexViewController()
            
        case .height:
            
            return HeightViewController()
        }
    }
}

struct HealthFeeds {
    
    var feeds: [HealthViewControllers]
}

class BodyHealthScrollViewController: UIViewController {
    
    //MARK: - Accessors
    
    lazy var healthFeeds: HealthFeeds = {
        
        var healthFeeds = HealthFeeds(feeds: [
            .weight,
            .bmi,
            .height
            ])
        
        return healthFeeds
    }()
    
    /// Scroll view to contain health view controllers.
    lazy var containerScrollView: UIScrollView = {
        
        let containerScrollView = UIScrollView.newAutoLayout()
        
        containerScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width * CGFloat(self.healthFeeds.feeds.count),
                                                 height: 0.0)
        
        containerScrollView.bounces = false
        containerScrollView.delegate = self
        containerScrollView.isPagingEnabled = true
        containerScrollView.canCancelContentTouches = false
        containerScrollView.delaysContentTouches = true
        
        return containerScrollView
    }()
    
    lazy var pageIndicatorView: PageIndicatorView = {
       
        var pageIndicatorView = PageIndicatorView(numberOfPages: self.healthFeeds.feeds.count)
        
        pageIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        pageIndicatorView.backgroundColor = UIColor.clear
        
        return pageIndicatorView
    }()
    
    //MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        view.addSubview(containerScrollView)
        view.addSubview(pageIndicatorView)
        
        updateViewConstraints()

        loadContainer()
    }
    
    //MARK: - Constraints
    
    override func updateViewConstraints() {
        
        super.updateViewConstraints()
        
        /*-------------------*/
        
        containerScrollView.autoPinEdge(toSuperviewEdge: .left)
        
        containerScrollView.autoPinEdge(toSuperviewEdge: .right)
        
        containerScrollView.autoPinEdge(toSuperviewEdge: .bottom)
        
        containerScrollView.autoPinEdge(toSuperviewEdge: .top,
                                                       withInset: -20.0)
        
        /*-------------------*/

        pageIndicatorView.autoAlignAxis(toSuperviewAxis: .vertical)
        
        pageIndicatorView.autoSetDimensions(to: CGSize(width: DotSizes.dotLayerSize * CGFloat(healthFeeds.feeds.count),
                                                       height: DotSizes.dotLayerSize))
        
        pageIndicatorView.autoPinEdge(toSuperviewEdge: .top,
                                      withInset: 20.0)
    }
    
    //MARK: - LoadContainer
    
    func loadContainer() {
        
        for index in 0..<healthFeeds.feeds.count  {
            
            guard let healthViewControllers = HealthViewControllers(rawValue: index) else {
                
                return
            }
            
            let viewController = healthViewControllers.healthViewController()
            
            let viewXPosition = UIScreen.main.bounds.width * CGFloat(index)
            
            let viewFrame = CGRect(x: viewXPosition,
                                   y: 0.0,
                                   width: UIScreen.main.bounds.width,
                                   height: UIScreen.main.bounds.height)
            
            addChildViewController(viewController,
                                   frame: viewFrame,
                                   view: containerScrollView)
        }
    }
}

extension BodyHealthScrollViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let relativePosition: CGFloat = scrollView.contentOffset.x / UIScreen.main.bounds.size.width
        
        pageIndicatorView.progress = relativePosition;

        scrollView.endEditing(true)
    }
}
