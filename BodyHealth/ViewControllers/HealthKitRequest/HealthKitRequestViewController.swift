//
//  HealthKitRequestViewController.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import HealthKit

class HealthKitRequestViewController: UIViewController {

    //MARK: - Accessors
    
    lazy var goButton : UIButton = {
        
        var goButton = UIButton.newAutoLayout()
        
        goButton.setTitleColor(UIColor.white,
                               for: .normal)
        
        goButton.setTitle(NSLocalizedString("GO", comment: "go_title"),
                          for: .normal)
        
        goButton.addTarget(self,
                           action: #selector(goButtonPressed(_:)),
                           for: .touchUpInside)
        
        goButton.backgroundColorForStates(normal: UIColor.buttercup,
                                          highlighted: UIColor.meteor)
        
        goButton.layer.cornerRadius = 5.0 * DeviceManager.sharedInstance.resizeFactor
        goButton.titleLabel?.font = UIFont.sanFranciscoUITextRegular(withSize: 45.0)
        goButton.backgroundColor = UIColor.buttercup
        
        return goButton
    }()
    
    lazy var informationLabel: PaddingLabel = {
        
        var informationLabel = PaddingLabel.newAutoLayout()
        
        informationLabel.font = UIFont.sanFranciscoUITextRegular(withSize: 35.0)
        informationLabel.text = NSLocalizedString("The App needs access to Health Kit, please grant it.", comment: "information_request_health_kit")
        informationLabel.textColor = UIColor.buttercup
        informationLabel.numberOfLines = 0
        informationLabel.minimumScaleFactor = 0.3
        informationLabel.adjustsFontSizeToFitWidth = true
        informationLabel.textAlignment = .center
        
        informationLabel.layer.cornerRadius = 5.0 * DeviceManager.sharedInstance.resizeFactor
        informationLabel.layer.borderColor = UIColor.buttercup.cgColor
        informationLabel.layer.borderWidth = 2.0 * DeviceManager.sharedInstance.resizeFactor
        
        informationLabel.leftInset = 15.0 * DeviceManager.sharedInstance.resizeFactor
        informationLabel.rightInset = 15.0 * DeviceManager.sharedInstance.resizeFactor
        
        return informationLabel
    }()
    
    //MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        view.addSubview(goButton)
        view.addSubview(informationLabel)
    
        updateViewConstraints()        
    }

    //MARK: - Constraints
    
    override func updateViewConstraints() {
        
        super.updateViewConstraints()
        
        /*-------------------*/
        
        informationLabel.autoPinEdge(toSuperviewEdge: .left,
                                     withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        informationLabel.autoPinEdge(toSuperviewEdge: .right,
                                     withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        informationLabel.autoPinEdge(toSuperviewEdge: .top,
                                     withInset: 30.0 * DeviceManager.sharedInstance.resizeFactor)
        
        informationLabel.autoPinEdge(.bottom,
                                     to: .top,
                                     of: goButton,
                                     withOffset: -10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        goButton.autoPinEdge(toSuperviewEdge: .left,
                             withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        goButton.autoPinEdge(toSuperviewEdge: .right,
                             withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        goButton.autoPinEdge(toSuperviewEdge: .bottom,
                             withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        goButton.autoSetDimension(.height,
                                  toSize: 200.0 * DeviceManager.sharedInstance.resizeFactor)
    }
    
    //MARK: - ButtonActions
    
    func goButtonPressed(_ sender: UIButton) {
        
        HealthKitManager.sharedInstance.authorizeHealthKit { (authorized,  error) -> Void in
            
            SessionManager.sharedInstance.loadSessionState()
            
            switch SessionManager.sharedInstance.state {
                
            case .closed:
                
                let instructionsViewController = InstructionsViewController()
                
                instructionsViewController.modalPresentationStyle = .overCurrentContext
                
                // Push to the main thread
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    self.present(instructionsViewController,
                                 animated: true,
                                 completion: nil)
                })
                
            case .unknown:
                
                ()
                
            case .open:
                
                // Navigate to BodyHealthScroll
                DispatchQueue.main.async {
                    
                    self.navigateToBodyHealthScroll()
                }
            }
        }
    }
    
    func navigateToBodyHealthScroll() {
        
        let bodyHealthScrollViewController = BodyHealthScrollViewController()
        
        if let navigationController = self.navigationController,
            let topViewController = navigationController.topViewController {
            
            if topViewController.isKind(of: BodyHealthScrollViewController.self) == false {
                
                navigationController.pushViewController(bodyHealthScrollViewController,
                                                         animated: true)
            }
        }
    }
}
