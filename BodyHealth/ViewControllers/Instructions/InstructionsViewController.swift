//
//  InstructionsViewController.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 21/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class InstructionsViewController: UIViewController {
    
    //MARK: - Accessors
    
    lazy var instructionsView: UIView = {
        
        var instructionsView = UIView.newAutoLayout()
        
        instructionsView.layer.borderColor = UIColor.dustyGray.cgColor
        instructionsView.layer.borderWidth = 2.0 * DeviceManager.sharedInstance.resizeFactor
        instructionsView.layer.cornerRadius = 15.0 * DeviceManager.sharedInstance.resizeFactor
        
        instructionsView.backgroundColor = UIColor.concrete
        
        return instructionsView
    }()
    
    lazy var swipeGestureRecognizer: UISwipeGestureRecognizer = {
        
        var swipeGestureRecognizer = UISwipeGestureRecognizer(target: self,
                                                              action: #selector(swipeGestureRecognizerSwipped(_:)))
        
        swipeGestureRecognizer.direction = .down
        
        return swipeGestureRecognizer
    }()
    
    lazy var titleLabel: UILabel = {
        
        var titleLabel = UILabel.newAutoLayout()
        
        titleLabel.text = NSLocalizedString("How do I connect to HealthKit?", comment: "")
        titleLabel.numberOfLines = 2
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.sanFranciscoUITextMedium(withSize: 22.0)
        
        return titleLabel
    }()
    
    lazy var instructionOneLabel: UILabel = {
        
        var instructionOneLabel = UILabel.newAutoLayout()
        
        instructionOneLabel.text = NSLocalizedString("Open the Apple Health app on your smartphone.", comment: "")
        instructionOneLabel.numberOfLines = 2
        instructionOneLabel.textAlignment = .left
        instructionOneLabel.font = UIFont.sanFranciscoUITextRegular(withSize: 15.0)
        
        return instructionOneLabel
    }()
    
    lazy var instructionTwoAttributedText: NSMutableAttributedString = {
        
        var instructionTwoAttributedText = NSMutableAttributedString(string:  NSLocalizedString("Tap ‘Sources’ at the bottom of the screen.", comment: ""))
        
        let range = instructionTwoAttributedText.mutableString.range(of: NSLocalizedString("‘Sources’", comment: ""),
                                                                      options:NSString.CompareOptions.caseInsensitive)
        
        instructionTwoAttributedText.addAttribute(NSForegroundColorAttributeName,
                                                   value: UIColor.radicalRed,
                                                   range: range)
        
        return instructionTwoAttributedText
    }()
    
    lazy var instructionTwoLabel: UILabel = {
        
        var instructionTwoLabel = UILabel.newAutoLayout()
        
        instructionTwoLabel.attributedText = self.instructionTwoAttributedText
        
        instructionTwoLabel.numberOfLines = 2
        instructionTwoLabel.textAlignment = .left
        instructionTwoLabel.font = UIFont.sanFranciscoUITextRegular(withSize: 15.0)
        
        return instructionTwoLabel
    }()
    
    lazy var instructionThreeLabel: UILabel = {
        
        var instructionThreeLabel = UILabel.newAutoLayout()
        
        instructionThreeLabel.text = NSLocalizedString("Select Body Health from the list of apps.", comment: "")
        instructionThreeLabel.numberOfLines = 2
        instructionThreeLabel.textAlignment = .left
        instructionThreeLabel.font = UIFont.sanFranciscoUITextRegular(withSize: 15.0)
        
        return instructionThreeLabel
    }()
    
    lazy var instructionFourAttributedText: NSMutableAttributedString = {
       
        var instructionFourAttributedText = NSMutableAttributedString(string:  NSLocalizedString("Select\n'Turn All Categories On'.", comment: ""))

        let range = instructionFourAttributedText.mutableString.range(of: NSLocalizedString("'Turn All Categories On'", comment: ""),
                                                                      options:NSString.CompareOptions.caseInsensitive)
        
        instructionFourAttributedText.addAttribute(NSForegroundColorAttributeName,
                                                   value: UIColor.radicalRed,
                                                   range: range)
        
        return instructionFourAttributedText
    }()
    
    lazy var instructionFourLabel: UILabel = {
        
        var instructionFourLabel = UILabel.newAutoLayout()
        
        instructionFourLabel.attributedText = self.instructionFourAttributedText
        
        instructionFourLabel.numberOfLines = 2
        instructionFourLabel.textAlignment = .left
        instructionFourLabel.font = UIFont.sanFranciscoUITextRegular(withSize: 15.0)
        
        return instructionFourLabel
    }()
    
    lazy var instructionOneIcon: UIImageView = {
       
        var instructionOneIcon = UIImageView.newAutoLayout()
        
        instructionOneIcon.image = UIImage(named: "health-app-icon")
        
        return instructionOneIcon
    }()
    
    lazy var instructionTwoIcon: UIImageView = {
        
        var instructionTwoIcon = UIImageView.newAutoLayout()
        
        instructionTwoIcon.image = UIImage(named: "sources-icon")
        
        return instructionTwoIcon
    }()
    
    lazy var instructionThreeIcon: UIImageView = {
        
        var instructionThreeIcon = UIImageView.newAutoLayout()
        
        instructionThreeIcon.image = UIImage(named: "bodyhealth-icon")
        
        return instructionThreeIcon
    }()
    
    lazy var instructionFourIcon: UIImageView = {
        
        var instructionFourIcon = UIImageView.newAutoLayout()
        
        instructionFourIcon.image = UIImage(named: "touch-icon")
        
        return instructionFourIcon
    }()
    
    //MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        view.addSubview(instructionsView)
        instructionsView.addSubview(titleLabel)
        instructionsView.addSubview(instructionOneLabel)
        instructionsView.addSubview(instructionTwoLabel)
        instructionsView.addSubview(instructionThreeLabel)
        instructionsView.addSubview(instructionFourLabel)
        instructionsView.addSubview(instructionOneIcon)
        instructionsView.addSubview(instructionTwoIcon)
        instructionsView.addSubview(instructionThreeIcon)
        instructionsView.addSubview(instructionFourIcon)
        
        view.addGestureRecognizer(swipeGestureRecognizer)
        
        view.backgroundColor = UIColor.scorpion.withAlphaComponent(0.6)
        
        updateViewConstraints()
    }
    
    //MARK: - Constraints
    
    override func updateViewConstraints() {
        
        super.updateViewConstraints()
        
        /*-------------------*/
        
        instructionsView.autoPinEdge(toSuperviewEdge: .top,
                                     withInset: 110.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionsView.autoPinEdge(toSuperviewEdge: .bottom,
                                     withInset: 110.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionsView.autoPinEdge(toSuperviewEdge: .left,
                                     withInset: 30.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionsView.autoPinEdge(toSuperviewEdge: .right,
                                     withInset: 30.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        titleLabel.autoPinEdge(toSuperviewEdge: .top,
                               withInset: 20.0 * DeviceManager.sharedInstance.resizeFactor)
        
        titleLabel.autoPinEdge(toSuperviewEdge: .left,
                               withInset: 20.0 * DeviceManager.sharedInstance.resizeFactor)
        
        titleLabel.autoPinEdge(toSuperviewEdge: .right,
                               withInset: 20.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        instructionOneLabel.autoPinEdge(toSuperviewEdge: .top,
                                        withInset: 100.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionOneLabel.autoPinEdge(toSuperviewEdge: .left,
                                        withInset: 60.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionOneLabel.autoPinEdge(toSuperviewEdge: .right,
                                        withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        instructionTwoLabel.autoPinEdge(toSuperviewEdge: .top,
                                        withInset: 160.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionTwoLabel.autoPinEdge(toSuperviewEdge: .left,
                                        withInset: 60.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionTwoLabel.autoPinEdge(toSuperviewEdge: .right,
                                        withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        instructionThreeLabel.autoPinEdge(toSuperviewEdge: .top,
                                          withInset: 220.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionThreeLabel.autoPinEdge(toSuperviewEdge: .left,
                                          withInset: 60.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionThreeLabel.autoPinEdge(toSuperviewEdge: .right,
                                          withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        instructionFourLabel.autoPinEdge(toSuperviewEdge: .top,
                                         withInset: 280.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionFourLabel.autoPinEdge(toSuperviewEdge: .left,
                                         withInset: 60.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionFourLabel.autoPinEdge(toSuperviewEdge: .right,
                                         withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        instructionOneIcon.autoAlignAxis(.horizontal,
                                         toSameAxisOf: instructionOneLabel)
        
        instructionOneIcon.autoPinEdge(toSuperviewEdge: .left,
                                        withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionOneIcon.autoSetDimensions(to: CGSize(width: 40.0 * DeviceManager.sharedInstance.resizeFactor,
                                                        height: 40.0 * DeviceManager.sharedInstance.resizeFactor))
        /*-------------------*/
        
        instructionTwoIcon.autoAlignAxis(.horizontal,
                                         toSameAxisOf: instructionTwoLabel)
        
        instructionTwoIcon.autoPinEdge(toSuperviewEdge: .left,
                                        withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionTwoIcon.autoSetDimensions(to: CGSize(width: 40.0 * DeviceManager.sharedInstance.resizeFactor,
                                                        height: 40.0 * DeviceManager.sharedInstance.resizeFactor))
        
        /*-------------------*/
        
        instructionThreeIcon.autoAlignAxis(.horizontal,
                                         toSameAxisOf: instructionThreeLabel)
        
        instructionThreeIcon.autoPinEdge(toSuperviewEdge: .left,
                                          withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        instructionThreeIcon.autoSetDimensions(to: CGSize(width: 40.0 * DeviceManager.sharedInstance.resizeFactor,
                                                          height: 40.0 * DeviceManager.sharedInstance.resizeFactor))
        /*-------------------*/
        
        instructionFourIcon.autoAlignAxis(.horizontal,
                                         toSameAxisOf: instructionFourLabel)
        
        instructionFourIcon.autoPinEdge(toSuperviewEdge: .left,
                                         withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)

        instructionFourIcon.autoSetDimensions(to: CGSize(width: 40.0 * DeviceManager.sharedInstance.resizeFactor,
                                                        height: 40.0 * DeviceManager.sharedInstance.resizeFactor))
    }
    
    //MARK: - GestureRecognizerActions
    
    func swipeGestureRecognizerSwipped(_ sender: UISwipeGestureRecognizer) {
        
        dismiss(animated: true,
                completion: nil)
    }
}
