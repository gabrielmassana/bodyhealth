//
//  RootNavigationController.swift
//  TraktTV
//
//  Created by GabrielMassana on 19/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class RootNavigationController: UINavigationController {

    //MARK: - Accessors
    
    /// RootViewController for the Navigation Controller.
    let rootViewController: UIViewController = {
        
        switch SessionManager.sharedInstance.state {
            
        case .closed,
             .unknown:
            
            return HealthKitRequestViewController()

        case .open:
            
            return BodyHealthScrollViewController()
        }
    }()
    
    //MARK: - Init
    
    init() {
        
        super.init(nibName: nil, bundle: nil)
        
        isNavigationBarHidden = true
        
        viewControllers = [rootViewController]
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
}
