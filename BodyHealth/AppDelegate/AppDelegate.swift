//
//  AppDelegate.swift
//  BodyHealth
//
//  Created by GabrielMassana on 25/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import Crashlytics
import Fabric

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    //MARK: - Accessors
    
    /// App Window
    var window: UIWindow? = {
        
        let window: UIWindow = UIWindow(frame: UIScreen.main.bounds)
        
        window.backgroundColor = UIColor.white
        
        return window
    }()
    
    /// The Navigation Controller working as rootViewController.
    lazy var rootNavigationController: RootNavigationController = {
        
        var rootNavigationController = RootNavigationController()
        
        return rootNavigationController
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        BuddyBuildSDK.setup()
        
        /*-------------------*/
        
        Fabric.sharedSDK().debug = false
        Fabric.with([Crashlytics.self,
                     Answers.self])
        
        /*-------------------*/

        window!.rootViewController = rootNavigationController
        window!.makeKeyAndVisible()
        
        return true
    }
}

