//
//  DotIndicatorLayer.swift
//  BodyHealth
//
//  Created by GabrielMassana on 31/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

internal struct DotSizes {
    
    /// Size of the space reserved for each dot indicator.
    static let dotLayerSize: CGFloat = 16.0 * DeviceManager.sharedInstance.resizeFactor
    
    /// Maximum Dot indicator Diameter.
    static let maxDiameter: CGFloat = 8.0 * DeviceManager.sharedInstance.resizeFactor
    
    /// Minimum Dot indicator diameter.
    static let minDiameter: CGFloat = 4.0 * DeviceManager.sharedInstance.resizeFactor
    
    /// Dot indicator Y padding.
    static let yPadding: CGFloat = 8.0 * DeviceManager.sharedInstance.resizeFactor
    
    /// Dot indicator X padding.
    static let xPadding: CGFloat = 8.0 * DeviceManager.sharedInstance.resizeFactor
}

class DotIndicatorLayer: CALayer {
    
    //MARK: - Accessors
    
    /// The scale of the IndicatorLayer. This is also used to determine the alpha of its fill color as it gets bigger.
    var scale: CGFloat? {
        
        didSet {
            
            // push the refresh
            setNeedsDisplay()
        }
    }
    
    //MARK: - Init
    
    override init() {
        
        super.init()
        
        allowsEdgeAntialiasing = true
        self.bounds = CGRect(x: 0.0,
                             y: 0.0,
                             width: DotSizes.maxDiameter + DotSizes.xPadding,
                             height: DotSizes.maxDiameter + DotSizes.yPadding)
        
        self.contentsScale = UIScreen.main.scale
        self.drawsAsynchronously = true
        
        // push the refresh
        setNeedsDisplay()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - CALayer
    
    override func draw(in ctx: CGContext) {
        
        guard let scale = self.scale else {
            
            return
        }
        
        ctx.beginPath()
        
        //The bigger the scale the bigger the circle - that's kind of what scale does.
        var circleDiameter: CGFloat = DotSizes.maxDiameter - ((DotSizes.minDiameter / 100.0) * scale)

        if circleDiameter < DotSizes.minDiameter  {
            
            circleDiameter = DotSizes.minDiameter
        }
        
        let circleCenter: CGPoint = CGPoint(x: bounds.midX,
                                            y: bounds.midY)
        
        //Circle
        ctx.beginPath()
        
        ctx.addArc(center: circleCenter,
                   radius: circleDiameter / 2.0,
                   startAngle: 0.0,
                   endAngle: 2.0 * CGFloat(M_PI),
                   clockwise: false)
        
        ctx.setLineWidth(1.0)
        
        ctx.setStrokeColor(UIColor.buttercup.cgColor)
        ctx.setFillColor(UIColor.buttercup.cgColor)
        
        ctx.closePath()
        ctx.fillPath()

        ctx.drawPath(using: CGPathDrawingMode.stroke)
    }
}
