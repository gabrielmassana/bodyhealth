//
//  PageIndicatorView.swift
//  BodyHealth
//
//  Created by GabrielMassana on 31/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class PageIndicatorView: UIView {
    
    //MARK: - Accessors

    /// The progress value to represent thorugh the dots.
    var progress: CGFloat {
        
        didSet {
            
            position = progress

            // push the refresh
            setNeedsLayout()
        }
    }
    
    /// The number of pages to represent on the control.
    let numberOfPages: Int
    
    
    /// Array to store all the dot indicators.
    var dotIndicators: [DotIndicatorLayer] = {
       
        var dotIndicators = [DotIndicatorLayer]()
        
        return dotIndicators
    }()
    
    /// Actual position of the scrollView, allow refresh the dots.
    var position: CGFloat?
    
    //MARK: - Init
    
    /**
     Initilizes the control to represent the number of pages.
     
     - Parameter numberOfPages: The number of pages to represent
     - Returns: self instance.
     */
    init(numberOfPages: Int) {
        
        self.numberOfPages = numberOfPages
        progress = 0.0
        
        super.init(frame: CGRect.zero)
        
        addDotIndicators()
        
        // push the refresh
        setNeedsLayout()
        layer.delegate = self;
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Adds a number of dots indicators to the paginator.
    func addDotIndicators() {
        
        for _ in 0..<numberOfPages {
            
            let dotIndicator = DotIndicatorLayer()
            
            dotIndicators.append(dotIndicator)
            layer.addSublayer(dotIndicator)
        }
    }
    
    /**
     Update the dot indicators for the layer.
     
     - Parameter layer: view layer.
     */
    func updateDotIndicators(_ layer: CALayer) {
        
        guard let position = position else {
            
            return
        }
        
        for (index, dotIndicator) in dotIndicators.enumerated() {

            dotIndicator.position = CGPoint(x: (DotSizes.dotLayerSize / 2.0) + (DotSizes.dotLayerSize * CGFloat(index)),
                                            y: layer.bounds.midY);
            
            let distance: CGFloat = fabs(CGFloat(index) - position)
            let scale: CGFloat = distance * 100.0;
            
            // push the refresh for each dot in the setter.
            dotIndicator.scale = scale;
        }
    }
    
    //MARK: - CALayerDelegate

    override func layoutSublayers(of layer: CALayer) {
        
        super.layoutSublayers(of: layer)
        
        updateDotIndicators(layer)
    }
}
