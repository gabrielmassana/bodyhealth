//
//  PaddingLabel.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 12/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class PaddingLabel: UILabel {
    
    var topInset: CGFloat = 7.0
    var bottomInset: CGFloat = 7.0
    var leftInset: CGFloat = 7.0
    var rightInset: CGFloat = 7.0
    
    override func drawText(in rect: CGRect) {
        
        let insets = UIEdgeInsets(top: topInset,
                                  left: leftInset,
                                  bottom: bottomInset,
                                  right: rightInset)
        
        super.drawText(in: UIEdgeInsetsInsetRect(rect,
                                                 insets))
    }
    
    override var intrinsicContentSize: CGSize {
        
        get {
            
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            
            return contentSize
        }
    }
}
