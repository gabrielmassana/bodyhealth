//
//  UIColor+BodyHealthTests.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//
//  Naming: http://chir.ag/projects/name-that-color/
//

import XCTest

@testable import BodyHealth

class UIColorBodyHealthTests: XCTestCase {
    
    //MARK: - Accessors
    
    //MARK: - SetUp
    
    override func setUp() {
        
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //MARK: - Scorpion
    
    func test_scorpion_notNil() {
        
        let color = UIColor.scorpion
        
        XCTAssertNotNil(color, "A valid scorpion Color wasn't created");
    }
    
    func test_scorpion_typeUIColor() {
        
        let color = UIColor.scorpion
        
        XCTAssertTrue(color.isKind(of: UIColor.self), "A valid UIColor wasn't created");
    }
    
    //MARK: - Buttercup
    
    func test_buttercup_notNil() {
        
        let color = UIColor.buttercup
        
        XCTAssertNotNil(color, "A valid buttercup Color wasn't created");
    }
    
    func test_buttercup_typeUIColor() {
        
        let color = UIColor.buttercup
        
        XCTAssertTrue(color.isKind(of: UIColor.self), "A valid UIColor wasn't created");
    }
    
    //MARK: - TorchRed
    
    func test_torchRed_notNil() {
        
        let color = UIColor.scorpion
        
        XCTAssertNotNil(color, "A valid torchRed Color wasn't created");
    }
    
    func test_torchRed_typeUIColor() {
        
        let color = UIColor.scorpion
        
        XCTAssertTrue(color.isKind(of: UIColor.self), "A valid UIColor wasn't created");
    }
    
}
