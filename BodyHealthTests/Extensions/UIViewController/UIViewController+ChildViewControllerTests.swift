//
//  UIViewController+ChildViewControllerTests.swift
//  BodyHealth
//
//  Created by GabrielMassana on 31/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import BodyHealth

class UIViewControllerChildViewControllerTests: XCTestCase {
    
    //MARK: - Accessors

    var parent: UIViewController?
    var child: UIViewController?
    var frame: CGRect?
    var scrollView: UIScrollView?
    
    //MARK: - SetUp

    override func setUp() {
        
        super.setUp()

        parent = UIViewController()
        child = UIViewController()
        scrollView = UIScrollView()
        frame = CGRect(x: 10,
                       y: 10,
                       width: 300,
                       height: 300)
    }
    
    override func tearDown() {
        
        parent = nil
        child = nil
        frame = nil
        scrollView = nil

        super.tearDown()
    }
    
    //MARK: - AddChildViewController

    func test_addChildViewController_has_superview() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!)
        
        XCTAssertNotNil(child?.view.superview, "Child not added to Parent Superview")
    }
    
    func test_addChildViewController_match_superview() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!)
        
        XCTAssertEqual(child?.view.superview, parent?.view, "Child added to different Parent Superview")
    }
    
    func test_addChildViewController_frame() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!)
        
        XCTAssertEqual(child?.view.frame, frame, "Child added to wrong superview frame")
    }
    
    func test_addChildViewController_contains() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!)
        
        XCTAssertTrue(parent!.childViewControllers.contains(child!), "Parent do not contain child View Controller")
    }
    
    //MARK: - AddChildViewController_ToView
    
    func test_addChildViewController_toView_has_superview() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!,
                                       view: scrollView!)
        
        XCTAssertNotNil(child?.view.superview, "Child not added to Parent Superview")
    }
    
    func test_addChildViewController_toView_match_superview() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!,
                                       view: scrollView!)
        
        XCTAssertEqual(child?.view.superview, scrollView, "Child added to different Parent Superview")
    }
    
    func test_addChildViewController_toView_frame() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!,
                                       view: scrollView!)
        
        XCTAssertEqual(child?.view.frame, frame, "Child added to wrong superview frame")
    }
    
    func test_addChildViewController_toView_contains() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!,
                                       view: scrollView!)
        
        XCTAssertTrue(parent!.childViewControllers.contains(child!), "Parent do not contain child View Controller")
    }
    
    //MARK: - RemoveFromParentViewControllerAndSuperview
    
    func test_removeFromParentViewControllerAndSuperview() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!)
        
        child?.removeFromParentViewControllerAndSuperview()
        
        XCTAssertNil(child?.view.superview, "Child not removed from Parent Superview")
    }
    
    func test_removeFromParentViewControllerAndSuperview_toView() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!,
                                       view: scrollView!)
        
        child?.removeFromParentViewControllerAndSuperview()
        
        XCTAssertNil(child?.view.superview, "Child not removed from Parent Superview")
    }
    
    func test_removeFromParentViewControllerAndSuperview_contains() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!)
        
        child?.removeFromParentViewControllerAndSuperview()

        XCTAssertFalse(parent!.childViewControllers.contains(child!), "Parent still contains child View Controller")
    }
    
    func test_removeFromParentViewControllerAndSuperview_toView_contains() {
        
        parent?.addChildViewController(child!,
                                       frame: frame!,
                                       view: scrollView!)
        
        child?.removeFromParentViewControllerAndSuperview()

        XCTAssertFalse(parent!.childViewControllers.contains(child!), "Parent still contains child View Controller")
    }
}
