//
//  Double+RoundedTests.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 11/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import BodyHealth

class DoubleRoundedTests: XCTestCase {
    
    //MARK: - Accessors
    
    var tenDecimals: Double?
    var fiveDecimals: Double?
    
    var roundedTwo: Double?
    var roundedFour: Double?

    //MARK: - SetUp
    
    override func setUp() {
        
        super.setUp()
        
        tenDecimals = 456.1234567891
        fiveDecimals = 456.12345
        
        roundedTwo = 456.12
        roundedFour = 456.1235
    }
    
    override func tearDown() {
        
        tenDecimals = nil
        fiveDecimals = nil
        
        roundedTwo = nil
        roundedFour = nil
        
        super.tearDown()
    }
    
    //MARK: - Round
    
    func test_tenDecimals_roundedTwo() {
        
        let rounded = tenDecimals!.roundTo(places: 2)
        
        XCTAssertEqual(rounded, roundedTwo!, "")
    }
    
    func test_fiveDecimals_roundedTwo() {
        
        let rounded = fiveDecimals!.roundTo(places: 2)
        
        XCTAssertEqual(rounded, roundedTwo!, "")
    }
    
    func test_tenDecimals_roundedFour() {
        
        let rounded = tenDecimals!.roundTo(places: 4)
        
        XCTAssertEqual(rounded, roundedFour!, "")
    }
    
    func test_fiveDecimals_roundedFour() {
        
        let rounded = fiveDecimals!.roundTo(places: 4)
        
        XCTAssertEqual(rounded, roundedFour!, "")
    }
}
