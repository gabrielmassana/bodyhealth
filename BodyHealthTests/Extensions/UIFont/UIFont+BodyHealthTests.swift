//
//  UIFont+BodyHealthTests.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 06/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import BodyHealth

class UIFontBodyHealthTests: XCTestCase {
    
    //MARK: - Accessors
    
    var size = 20.0
    
    //MARK: - SetUp
    
    override func setUp() {
        
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //MARK: - SanFranciscoUITextBold
    
    func test_sanFranciscoUITextBold_notNil() {
        
        let font = UIFont.sanFranciscoUITextBold(withSize: 20.0)
        
        XCTAssertNotNil(font, "A valid scorpion Color wasn't created");
    }
    
    func test_sanFranciscoUITextBold_typeUIFont() {
        
        let font = UIFont.sanFranciscoUITextBold(withSize: 20.0)
        
        XCTAssertTrue(font.isKind(of: UIFont.self), "A valid UIFont wasn't created");
    }
    
    //MARK: - SanFranciscoUITextRegular
    
    func test_sanFranciscoUITextRegular_notNil() {
        
        let font = UIFont.sanFranciscoUITextRegular(withSize: 20.0)
        
        XCTAssertNotNil(font, "A valid sanFranciscoUITextRegular Font wasn't created");
    }
    
    func test_sanFranciscoUITextRegular_typeUIFont() {
        
        let font = UIFont.sanFranciscoUITextRegular(withSize: 20.0)
        
        XCTAssertTrue(font.isKind(of: UIFont.self), "A valid UIFont wasn't created");
    }
}
