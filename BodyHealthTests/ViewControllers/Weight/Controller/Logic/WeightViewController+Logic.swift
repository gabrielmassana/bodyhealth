//
//  WeightViewController+LogicTests.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 10/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import BodyHealth

class MockDataSource: NSObject, UITableViewDataSource {
    
    var sections = [WeightSection(section: .main, rows: [.weight, .date, .submitButton])]

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sections[section].rows.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let rowType = sections[indexPath.section].rows[indexPath.row]
        
        return rowType.rowCell(delegate: WeightViewController(),
                               weightModel: WeightModel())
    }
}

class WeightViewControllerLogic: XCTestCase {
    
    //MARK: - Accessors

    var mockDataSource: MockDataSource?
    var tableView: UITableView?
    
    var weightViewController: WeightViewController?
    
    //MARK: - SetUp
    
    override func setUp() {

        super.setUp()

        mockDataSource = MockDataSource()
        tableView = UITableView()
        weightViewController = WeightViewController()
        weightViewController?.tableView = tableView!
    }
    
    override func tearDown() {
        
        mockDataSource = nil
        tableView = nil
        weightViewController = nil
        
        super.tearDown()
    }
    
    //MARK: - SubmitButtonCanUploadWeight

    func test_submitButtonCanUploadWeight_alpha_10() {
        
        tableView!.dataSource = mockDataSource
        tableView!.reloadData()
        weightViewController!.view.layoutIfNeeded()
        
        weightViewController!.submitButtonCanUploadWeight(canUpload: true)
        
        let row = weightViewController!.indexForRowType(type: .submitButton)
        let section = weightViewController!.indexForSectionType(type: .main)
        let cell = tableView?.cellForRow(at: IndexPath(row: row,
                                                      section: section)) as! SubmitButtonCell

        XCTAssertEqual(cell.submitButton.alpha, 1.0, "Wrong alpha on submitButton")
    }
    
    func test_submitButtonCanUploadWeight_alpha_05() {
        
        tableView!.dataSource = mockDataSource
        tableView!.reloadData()
        weightViewController!.view.layoutIfNeeded()
        
        weightViewController!.submitButtonCanUploadWeight(canUpload: false)
        
        let row = weightViewController!.indexForRowType(type: .submitButton)
        let section = weightViewController!.indexForSectionType(type: .main)
        let cell = tableView?.cellForRow(at: IndexPath(row: row,
                                                       section: section)) as! SubmitButtonCell
        
        XCTAssertEqual(cell.submitButton.alpha, 0.5, "Wrong alpha on submitButton")
    }
    
    //MARK: - CleanWeightValue
    
    func test_cleanWeightValue_text() {
        
        tableView!.dataSource = mockDataSource
        tableView!.reloadData()
        weightViewController!.view.layoutIfNeeded()
        
        weightViewController!.cleanWeightValue()
        
        let row = weightViewController!.indexForRowType(type: .weight)
        let section = weightViewController!.indexForSectionType(type: .main)
        let cell = tableView?.cellForRow(at: IndexPath(row: row,
                                                       section: section)) as! WeightCell
        
        XCTAssertEqual(cell.weightTextField.text, "", "Wrong text on weightTextField")
    }

    //MARK: - IndexForRowType
    
    func test_indexForRowType_weight() {
        
        let index = weightViewController?.indexForRowType(type: .weight)
        
        XCTAssertEqual(index, 0, "Wrong index for WeightRowType")
    }
    
    func test_indexForRowType_date() {
        
        let index = weightViewController?.indexForRowType(type: .date)
        
        XCTAssertEqual(index, 1, "Wrong index for WeightRowType")
    }
    
    func test_indexForRowType_submitButton() {
        
        let index = weightViewController?.indexForRowType(type: .submitButton)
        
        XCTAssertEqual(index, 2, "Wrong index for WeightRowType")
    }
    
    //MARK: - IndexForSectionType

    func test_indexForRowType_main() {
        
        let index = weightViewController?.indexForSectionType(type: .main)
        
        XCTAssertEqual(index, 0, "Wrong index for WeightSectionType")
    }
}
