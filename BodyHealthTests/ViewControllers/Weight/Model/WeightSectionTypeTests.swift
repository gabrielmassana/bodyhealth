//
//  WeightSectionTypeTests.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 07/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import BodyHealth

class WeightSectionTypeTests: XCTestCase {
    
    //MARK: - Accessors
    
    let main = WeightSectionType.main
    
    //MARK: - SetUp
    
    override func setUp() {
        
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //MARK: - SectionTitle
    
    func test_sectionTitle_main_notNil() {
        
        let title = main.sectionTitle()
        
        XCTAssertNotNil(title, "Main Section title should not be nil")
    }

    func test_sectionTitle_main() {
        
        let title = main.sectionTitle()
        
        XCTAssertEqual(title, NSLocalizedString("Weight", comment: "weight_title"), "Main Section title is worng")
    }
}
