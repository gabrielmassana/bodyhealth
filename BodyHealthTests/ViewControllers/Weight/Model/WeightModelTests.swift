//
//  WeightModelTests.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 10/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import BodyHealth

class WeightModelTests: XCTestCase {
    
    //MARK: - Accessors
    
    var weightModel: WeightModel?
    
    //MARK: - SetUp

    override func setUp() {
        
        super.setUp()

        weightModel = WeightModel()
    }
    
    override func tearDown() {
        
        weightModel = nil
        
        super.tearDown()
    }
    
    //MARK: - isWeightValid
    
    func test_isWeightValid_weight_nil() {
        
        weightModel?.weight = nil
        
        let valid = weightModel!.isWeightValid()
        
        XCTAssertFalse(valid, "weightModel instance should be no valid")
    }
    
    func test_isWeightValid_weight_not_nil() {
        
        weightModel?.weight = 50.0
        
        let valid = weightModel!.isWeightValid()
        
        XCTAssertTrue(valid, "weightModel instance should be valid")
    }
}
