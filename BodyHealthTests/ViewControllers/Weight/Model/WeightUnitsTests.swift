//
//  WeightUnitTests.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 08/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

import HealthKit

@testable import BodyHealth

class WeightUnitTests: XCTestCase {
    
    //MARK: - Accessors
    
    let kg = WeightUnit.kg
    let lbs = WeightUnit.lbs
    let st = WeightUnit.st
    
    var kgValue: Double?
    var gValue: Double?
    var lbsValue: Double?
    var stValue: Double?
    
    var kgQuantity: HKQuantity?
    var lbsQuantity: HKQuantity?
    var stQuantity: HKQuantity?
    
    //MARK: - SetUp
    
    override func setUp() {
        
        super.setUp()

        kgValue = 75
        gValue = kgValue! * 1000
        lbsValue = 165
        stValue = 20
        
        kgQuantity = HKQuantity(unit: .gram(),
                                doubleValue: gValue!)
        
        lbsQuantity = HKQuantity(unit: .pound(),
                                 doubleValue: lbsValue!)
        
        stQuantity = HKQuantity(unit: .stone(),
                                doubleValue: stValue!)
    }
    
    override func tearDown() {
        
        kgValue = nil
        gValue = nil
        lbsValue = nil
        stValue = nil
        
        kgQuantity = nil
        lbsQuantity = nil
        stQuantity = nil
        
        super.tearDown()
    }
    
    //MARK: - Quantity
    
    func test_quantity_kg_notNil() {
        
        let quantity = kg.quantity(weightQuantity: kgQuantity!)
        
        XCTAssertNotNil(quantity, "Double value should not be nil")
    }
    
    func test_quantity_kg_value() {
        
        let quantity = kg.quantity(weightQuantity: kgQuantity!)
        
        XCTAssertEqual(quantity, kgValue! , "Double value is wrong")
    }
    
    func test_quantity_lbs_notNil() {
        
        let quantity = lbs.quantity(weightQuantity: lbsQuantity!)
        
        XCTAssertNotNil(quantity, "Double value should not be nil")
    }
    
    func test_quantity_lbs_value() {
        
        let quantity = lbs.quantity(weightQuantity: lbsQuantity!)
        
        XCTAssertEqual(quantity, lbsValue! , "Double value is wrong")
    }
    
    func test_quantity_st_notNil() {
        
        let quantity = st.quantity(weightQuantity: stQuantity!)
        
        XCTAssertNotNil(quantity, "Double value should not be nil")
    }
    
    func test_quantity_st_value() {
        
        let quantity = st.quantity(weightQuantity: stQuantity!)
        
        XCTAssertEqual(quantity, stValue! , "Double value is wrong")
    }
    
    //MARK: - Unit
    
    func test_unit_kg_notNil() {
        
        let quantity = kg.unit(weight: kgValue!)
        
        XCTAssertNotNil(quantity, "HKQuantity should not be nil")
    }
    
    func test_unit_kg_value() {
        
        let quantity = kg.unit(weight: kgValue!)
        
        XCTAssertEqual(quantity.doubleValue(for: .gram()), gValue! , "HKQuantity value is wrong")
    }
    
    func test_unit_kg_unit_compatible() {
        
        let quantity = kg.unit(weight: kgValue!)
        
        XCTAssertTrue(quantity.is(compatibleWith: .gram()), "HKQuantity unit is wrong")
    }
    
    func test_unit_lbs_notNil() {
        
        let quantity = lbs.unit(weight: lbsValue!)
        
        XCTAssertNotNil(quantity, "HKQuantity should not be nil")
    }
    
    func test_unit_lbs_value() {
        
        let quantity = lbs.unit(weight: lbsValue!)
        
        XCTAssertEqual(quantity.doubleValue(for: .pound()), lbsValue! , "HKQuantity value is wrong")
    }
    
    func test_unit_lbs_unit_compatible() {
        
        let quantity = lbs.unit(weight: lbsValue!)
        
        XCTAssertTrue(quantity.is(compatibleWith: .pound()), "HKQuantity unit is wrong")
    }
    
    func test_unit_st_notNil() {
        
        let quantity = st.unit(weight: stValue!)
        
        XCTAssertNotNil(quantity, "HKQuantity should not be nil")
    }
    
    func test_unit_st_value() {
        
        let quantity = st.unit(weight: stValue!)
        
        XCTAssertEqual(quantity.doubleValue(for: .stone()), stValue! , "HKQuantity value is wrong")
    }
    
    func test_unit_st_unit_compatible() {
        
        let quantity = st.unit(weight: stValue!)
        
        XCTAssertTrue(quantity.is(compatibleWith: .stone()), "HKQuantity unit is wrong")
    }
    
    //MARK: - Row
    
    func test_row_kg_notNil() {
        
        let row = kg.row()
        
        XCTAssertNotNil(row, "Row should not be nil")
    }
    
    func test_row_kg() {
        
        let row = kg.row()
        
        XCTAssertEqual(row, 0, "Row is worng")
    }
    
    func test_row_lbs_notNil() {
        
        let row = lbs.row()
        
        XCTAssertNotNil(row, "Row should not be nil")
    }
    
    func test_row_lbs() {
        
        let row = lbs.row()
        
        XCTAssertEqual(row, 1, "Row is worng")
    }
    
    func test_row_st_notNil() {
        
        let row = st.row()
        
        XCTAssertNotNil(row, "Row should not be nil")
    }
    
    func test_row_st() {
        
        let row = st.row()
        
        XCTAssertEqual(row, 2, "Row is worng")
    }
}
