//
//  WeightRowTypeTests.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 07/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import BodyHealth

class WeightRowTypeTests: XCTestCase {
    
    //MARK: - Accessors
    
    let weight = WeightRowType.weight
    let date = WeightRowType.date
    let submitButton = WeightRowType.submitButton
    
    var delegate: WeightViewController?
    var weightModel: WeightModel?
    
    //MARK: - SetUp
    
    override func setUp() {
        
        super.setUp()

        delegate = WeightViewController()
        weightModel = WeightModel()
    }
    
    override func tearDown() {
        
        delegate = nil
        weightModel = nil
        
        super.tearDown()
    }
    
    //MARK: - RowCell

    func test_rowCell_weight() {
        
        let cell = weight.rowCell(delegate: delegate!,
                                  weightModel: weightModel!)
        
        XCTAssertTrue(cell.isKind(of: WeightCell.self), "cell type is worng, must be WeightCell")
    }
    
    func test_rowCell_weight_delegate() {
        
        let cell = weight.rowCell(delegate: delegate!,
                                  weightModel: weightModel!) as! WeightCell
        
        XCTAssertNotNil(cell.delegate, "cell delegate is nil")
    }
    
    func test_rowCell_date() {
        
        let cell = date.rowCell(delegate: delegate!,
                                  weightModel: weightModel!)
        
        XCTAssertTrue(cell.isKind(of: DateCell.self), "cell type is worng, must be DateCell")
    }
    
    func test_rowCell_submitButton() {
        
        let cell = submitButton.rowCell(delegate: delegate!,
                                  weightModel: weightModel!)
        
        XCTAssertTrue(cell.isKind(of: SubmitButtonCell.self), "cell type is worng, must be SubmitButtonCell")
    }
    
    func test_rowCell_submitButton_delegate() {
        
        let cell = submitButton.rowCell(delegate: delegate!,
                                  weightModel: weightModel!) as! SubmitButtonCell
        
        XCTAssertNotNil(cell.delegate, "cell delegate is nil")
    }
}
