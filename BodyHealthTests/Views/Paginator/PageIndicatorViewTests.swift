//
//  PageIndicatorViewTests.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 15/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import BodyHealth

class PageIndicatorViewTests: XCTestCase {
    
    //MARK: - Accessors
    
    static let numberOfPages5: Int = 5

    var pageIndicatorView: PageIndicatorView?

    //MARK: - SetUp
    
    override func setUp() {
        
        super.setUp()
    }
    
    override func tearDown() {
        
        pageIndicatorView = nil
        
        super.tearDown()
    }
    
    //MARK: - Init
    
    func test_init_layer_delegate() {
        
        pageIndicatorView = PageIndicatorView(numberOfPages: PageIndicatorViewTests.numberOfPages5)
        
        XCTAssertNotNil(pageIndicatorView?.layer.delegate, "PageIndicatorView Layer delegate is nil")
    }
    
    func test_init_addDotIndicators_dotIndicators_count() {
        
        pageIndicatorView = PageIndicatorView(numberOfPages: PageIndicatorViewTests.numberOfPages5)
        
        XCTAssertEqual(PageIndicatorViewTests.numberOfPages5, pageIndicatorView?.dotIndicators.count, "Wrong number of dot indicators")
    }
    
    func test_init_addDotIndicators_sublayers_count() {
        
        pageIndicatorView = PageIndicatorView(numberOfPages: PageIndicatorViewTests.numberOfPages5)
        
        XCTAssertEqual(PageIndicatorViewTests.numberOfPages5, pageIndicatorView?.layer.sublayers!.count, "Wrong number of sublayers")
    }
}
