//
//  DotIndicatorLayerTests.swift
//  BodyHealth
//
//  Created by Gabriel Massana on 15/11/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import BodyHealth

class DotIndicatorLayerTests: XCTestCase {
    
    //MARK: - Accessors
    
    //MARK: - SetUp
    
    override func setUp() {
        
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
